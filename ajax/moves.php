<?php
date_default_timezone_set("Europe/London");
//first, connect to the database and get the details  we're after.

$link = mysqli_connect("localhost","moves","qWwWjsscbtEn8fWp","moves") or die("");

$pull_from = 0; //if 1, pull from website_steps table instead
//query

$steps = 0;

if($pull_from) {
  $data = mysqli_fetch_array(mysqli_query($link, "SELECT * FROM `website_steps` ORDER BY epoch DESC LIMIT 0,1")); // or die("crap." . mysqli_error($link));
  $steps = $data['steps'];
  $distance = round($data['distance'] / 1000, 2);
} else {
  $data = mysqli_fetch_array(mysqli_query($link, "SELECT * FROM `users`")); // or die("crap." . mysqli_error($link));

  //make the call to the API

  $base_URL = "https://api.moves-app.com/api/1.1";

  $today = date("Ymd");
  $json = file_get_contents($base_URL . "/user/summary/daily/" . $today . "?access_token=" . $data['access_token']);

  $dump = json_decode($json, true);


  if($dump[0]['summary'][0]['steps'] == 0) {
  $steps = 0;
  $distance = 0;
  } else {
  $steps = $dump[0]['summary'][0]['steps'];
  $distance = round($dump[0]['summary'][0]['distance'] / 1000, 2);
  }

}

echo "Here's a fun fact - so far today I've walked " . $distance . "km, or ". round($steps,0) . " steps!" ;

?>
