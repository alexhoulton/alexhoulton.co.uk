<!DOCTYPE html>
<!--[if lt IE 7]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class='no-js' lang='en'>
	<!--<![endif]-->
	<head>
		<meta charset='utf-8' />
		<meta content='IE=edge,chrome=1' http-equiv='X-UA-Compatible' />
		<meta name="Description" content="I'm a CCNP certified network engineer based in London, England. I have a professional background in Computer Networking, and have a First Class Honours degree in Computer Networks from Sheffield Hallam University.">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title>Alex Houlton</title>
		<meta name="distribution" content="global" />

		<meta name="language" content="en" />
		<meta content='width=device-width, initial-scale=1.0, user-scalable=no' name='viewport' />

    <link href="https://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Raleway:400,100,200,300,600,500,700,800,900" rel="stylesheet" type="text/css">

		<script src="/assets/jquery/dist/jquery.min.js"></script>
		<script src="/assets/jquery-lazy/jquery.lazy.min.js"></script>

		<link rel="stylesheet" href="/dist/css/main.css?<?=time()?>" type="text/css" media="screen"/>
		<link href="/assets/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- Bootstrap -->
    <link href="/assets/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
		<script src="/assets/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	</head>
	<body id="top">
		<!-- Fixed navbar -->
<nav class="navbar navbar-inverse navbar-fixed-top">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="#top">Alex Houlton</a>
		</div>
		<div id="navbar" class="navbar-collapse collapse">
			<ul class="nav navbar-nav navbar-right">
				<li id="about-menu-item"><a href="#about">About</a></li>
				<li id="experience-menu-item"><a href="#experience">Experience</a></li>
				<li id="projects-menu-item"><a href="#projects">Projects</a></li>
				<li id="education-menu-item"><a href="#education">Education</a></li>
				<li id="contact-menu-item"><a href="#contact">Contact</a></li>
				<li><a href="/blog/">Blog</a></li>
			</ul>
		</div>
	</div>
</nav>
		<header>
        <div class="intro-content">
            <div class="title-name">Alex Houlton</div>
            <hr class="colored">
            <div>CCNP Certified Network Engineer and web developer</div>
        </div>
        <div class="scroll-down">
            <a class="btn page-scroll" href="#about"><i class="fa fa-angle-down fa-fw"></i></a>
        </div>
    </header>

		<section id="about">
			<div class="container">
				<div class="row">
                <div class="col-md-6">
                    <img data-src="css.png" class="img-responsive lazy" alt="">
                </div>
                <div class="col-md-6 text-center">
                    <h1>Hi, I'm Alex Houlton!</h1>
                    <hr class="colored">
                    <p>I'm a CCNP certified network engineer and web developer based in London, England. I have a professional background in Computer Networking, and have a First Class Honours degree in Computer Networks from Sheffield Hallam University.</p>
										<p>Please feel free to <a href="mailto:questions@alexhoulton.co.uk"> get in touch</a> if you have any questions.</p>
										<p id="moves_text" onclick="document.location='https://moveshistory.alexhoulton.co.uk/user/51838080216765000';"></p>
                </div>
            </div>
			</div>
		</section>
		<section id="experience">
        <div class="container-fluid">
            <div class="row text-center">
                <div class="col-lg-12">
                    <h1>Experience</h1>
                    <p>I specialise in computer networking and web development.</p>
                    <hr>
                </div>
            </div>
						<div class="row text-center content-row odd-row">
								<div class="col-md-2"></div>
								<div class="col-md-8">
                    <div class="about-content">
                        <img class="wider_logo lazy" data-src="/dist/images/ciscomeraki.png"></img>
                        <h3 class="nocaps">Network Support Engineer</h3>
												<h5 class="grey">June 2017 - Present</h1>
                        <p>At Cisco Meraki, my role focuses on providing quality support to our largest customers. </p>

												<p>This requires me to:</p>

												<ul class="left_align">
													<li>Analyse packet captures to quickly diagnose the root cause of problems.</li>
													<li>Effectively manage cases from start to finish, seeing every issue through to resolution.</li>
													<li>Familiarise oneself with the entire product line, including the hardware and software feature sets. This allows questions to be answered without delay and establish expected behaviour.</li>
													<li>Maintain deep knowledge of all Layer 1 through 4 protocols, including 802.11, Ethernet, IPv4/6, OSPF and TCP/UDP, in addition to application layer protocols.</li>
													<li>Contribute towards public facing documentation, ensuring that it is up to date and as clear as possible.</li>
													<li>Communicate professionally and precisely with customers and internal teams to ensure clear next steps are formed, which lead to a rapid resolution of the problem.</li>
												</ul>
												<p>As part of my role, I am part of a small team responsible for management of our internal lab space.</p>
                    </div>
                </div>
								<div class="col-md-2"></div>
            </div>
            <div class="row text-center content-row even-row">
								<div class="col-md-2"></div>
								<div class="col-md-8">
                    <div class="about-content">
                        <img class="logo lazy" data-src="/dist/images/cisco.png"></img>
                        <h3 class="nocaps">Solution Validation Services (SVS) Intern</h3>
												<h5 class="grey">July 2015 - June 2017</h1>
                        <p>In my position as an SVS Intern at Cisco, I have two main roles:</p>

												<p class="bold">Programmer, Next Generation Tooling (Lions NG)</p>

												<ul class="left_align">
													<li>Ensure HA through the use of load balancing (nginx, haproxy) and geographically diverse database and web servers.</li>
													<li>Effectively use version control solutions (Git) to track code changes, issues, branch code and develop efficiently within a team.</li>
													<li>Familiarity with both server side technologies (PHP, MySQL and Nginx in a Linux environment) and client side technologies (jQuery, DataTables, Bootstrap).</li>
													<li>Hardware control (Smart PDUs, networking equipment) through SNMP get, set and walk, allows us to power schedule projects, averaging a 70% power saving.</li>
													<li>Design, Implement, Test and Deploy new features to enhance the tools used on a daily basis by Lab Administrators and Engineers.</li>
													<li>Maintain and refactor the existing codebase as issues arise and workflows change.</li>
												</ul>
												<p class="bold">Lab Administrator, Advanced Services</p>
												<ul class="left_align">
													<li>Monitor case queues for new requests, acting upon these in line with SLAs. This could involve troubleshooting a faulty link or provisioning a new device into a topology.</li>
													<li>Asset Management, including assignment of devices to projects, e-wasting of old equipment.</li>
													<li>Work with Engineers to deploy a topology into the lab, including sourcing hardware, racking + stacking, configuring management and console access.</li>
													<li>Establishing layer 1 through 3 connectivity between devices as per provided topology diagram.</li>
													<li>Provision of traffic generation capability (Ixia) to a test topology.</li>
													<li>Maintenance of VMware/UCS-based virtual machine infrastucture for test purposes. Experience with Cisco UCSM, FI and vCenter.</li></li>
												</ul>
                    </div>
                </div>
								<div class="col-md-2"></div>
            </div>
						<div class="row text-center content-row odd-row">
								<div class="col-md-2"></div>
								<div class="col-md-8">
                    <div class="about-content">
                        <img class="logo lazy" data-src="/dist/images/sd.png"></img>
                        <h3 class="nocaps">IT Support Analyst</h3>
												<h5 class="grey">November 2013 - June 2015</h1>
                        <p>In my position as an IT Support Analyst, my duties included:</p>
												<ul class="left_align">
													<li>Handle and take ownership of calls and support tickets that arrive from users which include 800+ retail branches, warehouses and Head Office personnel.</li>
													<li>Support a broad range of hardware, from Point of Sale, handhelds, PC and Macintosh desktops/laptops to Cisco Routers, Switches and Access Points.</li>
													<li>Systems software such as Windows Server 2008/2012 (inc. AD, DNS, DHCP), VMware vSphere, Citrix Xenapp, XenDesktop and SCSM 2012 for incident management.</li>
													<li>End user software support includes Windows XP/7/8, Mac OS X, Office and Adobe CS6.</li>
													<li>Microsoft SQL Server and Oracle Databases play a heavy role in a retail business, from transaction data to customer details. This requires me to understand how they work and be able to maintain them effectively.</li>
													<li>Complex issues such as a network problem or a corrupt database have taught me teamwork skills. I escalate the problem, working with others to resolve the issue quickly, ensuring minimal downtime.</li>
													<li>Liaising with other companies, a courier for example, to ensure that a 4G/3G backup router is configured and shipped to a store whose MPLS link is down. Contact with the ISP is then made to fix the main link.</li>
												</ul>
                    </div>
                </div>
								<div class="col-md-2"></div>
            </div>
        </div>
    </section>
		<aside class="lazy sj-quote" data-src="../../sj.jpg">
        <div class="container">
            <div class="row">
							<div class="col-md-2"></div>
              <div class="col-md-4 quote">
                  <span>"because the people who are crazy enough to think they can change the world, are the ones who do."</span>
              </div>
							<div class="col-md-6"></div>
            </div>
        </div>
    </aside>
		<section id="projects">
        <div class="container-fluid">
            <div class="row text-center">
                <div class="col-lg-12">
                    <h1>Projects</h1>
                    <p>A few projects I have worked on recently:</p>
                    <hr>
                </div>
            </div>
						<div class="row text-center content-row odd-row">
								<div class="col-md-2"></div>
								<div class="col-md-4">
										<div class="about-content">
												<a href="https://moveshistory.alexhoulton.co.uk" target="_blank"><img class="cert lazy" data-src="/dist/images/mh.png"></img></a>
												<h3 class="nocaps"><a href="https://moveshistory.alexhoulton.co.uk" target="_blank">Moves History</a></h3>
												<h5 class="grey">A quick and easy to use web-app designed to show you historical information from your <a href="https://www.movesapp.com" target="_blank">Moves</a> data, such as last 7 or 31 day totals and weekly performance graphs.</h1>
										</div>
								</div>
								<div class="col-md-4">
										<div class="about-content">
												<a href="https://tubemap.alexhoulton.co.uk" target="_blank"><img class="cert lazy" data-src="/dist/images/tubemap.jpg"></img></a>
												<h3 class="nocaps"><a href="https://tubemap.alexhoulton.co.uk" target="_blank">Tube Map Challenge</a></h3>
												<h5 class="grey">No more paper! Digitally check off all the stations as you visit them! Integrates with <a href="https://www.swarmapp.com" target="_blank">Swarm</a> and TFL data to make <a href="https://tubemap.alexhoulton.co.uk/user/1" target="_blank">magic happen</a>.</h1>
												<h5 class="grey">This project is open source. You can <a href="https://gitlab.com/alexhoulton/tubechallenge" target="_blank">view and contribute</a> over on Gitlab.</h1>
										</div>
								</div>
								<div class="col-md-2"></div>
						</div>
        </div>
    </section>
		<section id="education">
        <div class="container-fluid">
            <div class="row text-center">
                <div class="col-lg-12">
                    <h1>Education</h1>
                    <p>I have a First Class BSc (Hons) degree in Network Management & Design.</p>
                    <hr>
                </div>
            </div>
            <div class="row text-center content-row odd-row">
								<div class="col-md-2"></div>
								<div class="col-md-8">
                    <div class="about-content">
                        <img class="logo lazy" data-src="/dist/images/shu.png"></img>
                        <h3 class="nocaps">BSc (Hons) Network Management & Design</h3>
												<h5 class="grey">September 2013 - June 2017</h1>
                        <p>As part of my degree at Sheffield Hallam, I have studied a wide range of topics in addition to the core networking area, giving me a broad range of knowledge on many aspects of the industry.</p>
												<p class="bold left_align">Second Year Modules include:</p>
												<ul class="left_align">
													<li>LAN Switching + WANs (follows Cisco CCNA 5 curriculum) - 94%</li>
													<li>Web Application Development - 89%</li>
													<li>Database Administration + Security - 98%</li>
													<li>Network Server Management + Administration - 86%</li>
												</ul>
												<p class="bold left_align">First Year Modules include:</p>
												<ul class="left_align">
													<li>Networking Fundamentals (follows Cisco CCNA 5 curriculum) - 89%</li>
													<li>C# Programming - 82%</li>
													<li>Computer Architecture - 93%</li>
													<li>Information Security - 88%</li>
												</ul>
                    </div>
                </div>
								<div class="col-md-2"></div>
            </div>
						<div id="certifications" class="row text-center">
                <div class="col-lg-12">
                    <h2>Certifications</h1>
                </div>
            </div>
						<div class="row text-center content-row even-row">
								<div class="col-md-2"></div>
								<div class="col-md-4">
                    <div class="about-content">
                        <img class="cert lazy" data-src="/dist/images/ccna.jpg"></img>
                        <h3 class="nocaps"><a href="https://www.youracclaim.com/badges/bb696529-01c1-4d1e-ab64-cbcf61b57d04/public_url" target="_blank">Cisco Certified Network Associate (CCNA R&S)</a></h3>
												<h5 class="grey">June 2015 - Present</h1>
                    </div>
                </div>
								<div class="col-md-4">
                    <div class="about-content">
                        <img class="cert lazy" data-src="/dist/images/ccnp.jpg"></img>
                        <h3 class="nocaps"><a href="https://www.youracclaim.com/badges/6d2f6a24-dfab-464c-b77c-18a76e2e827f/public_url" target="_blank">Cisco Certified Network Professional (CCNP R&S)</a></h3>
												<h5 class="grey">May 2016 - Present</h1>
                    </div>
                </div>
								<div class="col-md-2"></div>
            </div>
        </div>
    </section>
		<footer class="footer" id="contact">
        <div class="container text-center">
            <div class="row footer-social">
                <div class="col-lg-12">
                    <ul class="social-links list-inline">
                        <li>
													<a href="http://www.twitter.com/madnutter56"><i class="fa fa-twitter fa-fw fa-2x"></i></a>
                        </li>
                        <li>
													<a href="https://uk.linkedin.com/in/houltonalex"><i class="fa fa-linkedin fa-fw fa-2x"></i></a>
                        </li>
												<li>
													<a href="mailto:questions@alexhoulton.co.uk"><i class="fa fa-envelope fa-fw fa-2x"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="row copyright">
                <div class="col-lg-12">
                    <p class="small">© 2018 Alex Houlton</p>
                </div>
            </div>
        </div>
    </footer>
<script type="text/javascript">
//Smooth anchor scrolling
$(function() {
  $('a[href*="#"]:not([href="#"])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
			if($(this).closest('ul').hasClass('nav')) {
				$(this).closest('ul').children('li').each(function() { $(this).removeClass('active');});
				$(this).parent().addClass('active');
			}
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html, body').animate({
          scrollTop: target.offset().top
        }, 1000);
        return false;
      }
    }
  });
});

$(function() {
    $('.lazy').Lazy();
});

$(function(){
	// Load Moves Data
	// $.ajax({
	// 	url: "/ajax/moves.php",
	// 	cache: false
	// }).done(function( html ) {
	// 	$("#moves_text").append(html);
	// });

	var about_distance = $('#about').offset().top;
	var experience_distance = $('#experience').offset().top;
	var projects_distance = $('#projects').offset().top;
	var education_distance = $('#education').offset().top;
	var contact_distance = $('#certifications').offset().top;
    $window = $(window);

	$window.scroll(function() {
	    if ( $window.scrollTop() >= about_distance && $window.scrollTop() <= experience_distance) {
				$('#navbar').children('ul').first().children('li').each(function() { $(this).removeClass('active'); $(this).blur();});
	      $('#about-menu-item').addClass('active');
	    } else {
				if ( $window.scrollTop() >= experience_distance && $window.scrollTop() <= projects_distance ) {
				$('#navbar').children('ul').first().children('li').each(function() { $(this).removeClass('active'); $(this).blur();});
	      $('#experience-menu-item').addClass('active');
		    } else {
					if ( $window.scrollTop() >= projects_distance && $window.scrollTop() <= education_distance ) {
					$('#navbar').children('ul').first().children('li').each(function() { $(this).removeClass('active'); $(this).blur();});
		      $('#projects-menu-item').addClass('active');
			    } else {
						if ( $window.scrollTop() >= education_distance && $window.scrollTop() <= contact_distance ) {
						$('#navbar').children('ul').first().children('li').each(function() { $(this).removeClass('active'); $(this).blur();});
			      $('#education-menu-item').addClass('active');
				    } else {
							if ( $window.scrollTop() >= contact_distance ) {
								$('#navbar').children('ul').first().children('li').each(function() { $(this).removeClass('active'); $(this).blur();});
					      $('#contact-menu-item').addClass('active');
							} else {
								$('#navbar').children('ul').first().children('li').each(function() { $(this).removeClass('active'); $(this).blur();});
							}
						}
					}

				}
			}
	});
});

</script>
<script type="text/javascript">
var sc_project=8209508;
var sc_invisible=1;
var sc_security="83623bf1";
</script>
<script type='text/javascript' src='https://statcounter.com/counter/counter.js'></script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-37378135-1', 'auto');
  ga('send', 'pageview');

</script>
  </body>
</html>
